package dao;

import model.Employee;
import model.Role;
import model.Transaction;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class TransactionDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public int add(Transaction transaction) {
        entityManager.persist(transaction);
        entityManager.flush();
        return transaction.getId();
    }

    public List<Transaction> fetchAll() throws NoResultException {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Transaction> criteria = builder.createQuery(Transaction.class);
        Root<Transaction> from = criteria.from(Transaction.class);
        criteria.select(from);
        return entityManager.createQuery(criteria).getResultList();
    }

    public List<Transaction> fetchByToEmployee(int id) throws NoResultException {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Transaction> criteria = builder.createQuery(Transaction.class);

        Root<Transaction> from = criteria.from(Transaction.class);
        Join<Transaction, Employee> toEmployee = from.join("toEmployee");

        criteria.where(builder.equal(toEmployee.get("id"), id));

        return entityManager.createQuery(criteria).getResultList();
    }

}
