var page = null;

$(document).ready(function() {
    page = $('.page-register-employee');

    page.find('.select-roles').change(function(e){
        onRoleChange(this, e);
    });
});

function onRoleChange(obj, e) {
    page.find('.select-supervisors').removeClass('show').hide();
    page.find('.select-coordinators').removeClass('show').hide();

    page.find('.select-supervisors select').removeAttr("name");
    page.find('.select-coordinators select').removeAttr("name");

    var sel = null;

    if ($(obj).val() == 1) {
        sel = page.find('.select-supervisors');
    }

    if ($(obj).val() == 2) {
        sel = page.find('.select-coordinators');
    }

    sel.find('select').attr("name", "superior.id");
    sel.show();
}
