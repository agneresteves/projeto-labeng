package dao;

import model.Measure;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public class MeasureDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public void add(Measure Measure) {
        this.entityManager.persist(Measure);
    }

    public List<Measure> fetchAll() throws NoResultException {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Measure> criteria = builder.createQuery(Measure.class);
        Root<Measure> from = criteria.from(Measure.class);
        criteria.select(from);
        return entityManager.createQuery(criteria).getResultList();
    }

}
