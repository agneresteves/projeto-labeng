package controller;

import form.SearchForm;
import model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import service.EmployeeService;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/painel")
public class DashboardController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("")
    public ModelAndView home(HttpSession httpSession) {
        Employee employee = (Employee) httpSession.getAttribute("employee");
        HashMap<String, Object> params = employeeService.getDashParamsByRole(employee);
        return new ModelAndView(employeeService.getDashViewByRole(employee.getRole().getId()), params);
    }

    @GetMapping("/sair")
    public ModelAndView logout(HttpSession httpSession) {
        httpSession.removeAttribute("employee");
        return new ModelAndView("redirect:/login");
    }

    @PostMapping("/pesquisar")
    public ModelAndView pesquisar(SearchForm searchForm, HttpSession httpSession) {
        Employee employeeLogged = (Employee) httpSession.getAttribute("employee");

        HashMap<String, Object> params = new HashMap<String, Object>();

        params.put("employees", this.employeeService.find(searchForm.getName(), employeeLogged));
        params.put("search", true);
        return new ModelAndView("employee/list", params);
    }

}
