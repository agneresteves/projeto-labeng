--drop database system_tools;
create database system_tools;
use system_tools;

CREATE TABLE categories (
 id INT PRIMARY KEY AUTO_INCREMENT,
 name VARCHAR(50),
 created_at TIMESTAMP DEFAULT NOW(),
 updated_at TIMESTAMP,
 deleted_at TIMESTAMP
);

CREATE TABLE measures (
 id INT PRIMARY KEY AUTO_INCREMENT,
 name VARCHAR(50),
 created_at TIMESTAMP DEFAULT NOW(),
 updated_at TIMESTAMP,
 deleted_at TIMESTAMP
);


CREATE TABLE roles (
 id INT PRIMARY KEY AUTO_INCREMENT,
 name VARCHAR(50)
);


CREATE TABLE employees (
 id INT PRIMARY KEY AUTO_INCREMENT,
 roles_id INT,
 superior_id INT,
 name VARCHAR(100) NOT NULL,
 email VARCHAR(100) NOT NULL,
 password VARCHAR(100) NOT NULL,
 created_at TIMESTAMP DEFAULT NOW(),
 updated_at TIMESTAMP,
 deleted_at TIMESTAMP
);

CREATE TABLE items (
 id INT PRIMARY KEY AUTO_INCREMENT,
 name VARCHAR(50),
 quantity_default FLOAT,
 categories_id INT,
 measures_id INT,
 created_at TIMESTAMP DEFAULT NOW(),
 updated_at TIMESTAMP,
 deleted_at TIMESTAMP
);

create table transactions (
 id INT PRIMARY KEY AUTO_INCREMENT,
 confirmed BOOLEAN DEFAULT FALSE,
 do_employees_id INT,
 FOREIGN KEY(do_employees_id) REFERENCES employees(id),
 to_employees_id INT,
 FOREIGN KEY(to_employees_id) REFERENCES employees(id),
 code INT, -- gerado para assinar
 operation CHAR(1),
 observation TEXT,
 order_code VARCHAR(15),
 warehouse boolean, -- liberação 
 created_at TIMESTAMP DEFAULT NOW(),
 updated_at TIMESTAMP,
 deleted_at TIMESTAMP
);

select * from transactions;

CREATE TABLE transactions_items (
 id INT PRIMARY KEY AUTO_INCREMENT,
 quantity FLOAT,
 items_id INT,
 FOREIGN KEY(items_id) REFERENCES items(id),
 transactions_id INT,
 FOREIGN KEY(transactions_id) REFERENCES transaction(id),
 created_at TIMESTAMP DEFAULT NOW(),
 updated_at TIMESTAMP,
 deleted_at TIMESTAMP
);

ALTER TABLE employees ADD CONSTRAINT FK_employees_0 FOREIGN KEY (roles_id) REFERENCES roles (id);
ALTER TABLE employees ADD CONSTRAINT FK_employees_1 FOREIGN KEY (superior_id) REFERENCES employees (id);


ALTER TABLE items ADD CONSTRAINT FK_items_0 FOREIGN KEY (categories_id) REFERENCES categories (id);
ALTER TABLE items ADD CONSTRAINT FK_items_1 FOREIGN KEY (measures_id) REFERENCES measures (id);

insert into roles (name) values ('Técnico');
insert into roles (name) values ('Supervisor');
insert into roles (name) values ('Almoxarifado');
insert into roles (name) values ('Coordenador');

insert into employees (roles_id, superior_id, name, email, password) values (4, 0, 'Coordenador', 'coordenador@email.com', '12345');

insert into measures (name) values ('Unidade');
insert into measures (name) values ('Metros');

insert into categories (name) values ('Fio');
