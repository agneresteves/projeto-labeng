package dao;


import model.Employee;
import model.Item;
import model.Role;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.List;

@Transactional
@Repository
public class EmployeeDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public void add(Employee employee) {
        this.entityManager.persist(employee);
    }

    public CriteriaBuilder getBuilder() {
        return entityManager.getCriteriaBuilder();
    }

    public CriteriaQuery getQuery() {
        return getBuilder().createQuery(Employee.class);
    }

    public Employee findByEmailAndPassword(String email, String password) throws NoResultException {
        CriteriaQuery<Employee> criteria = this.getQuery();

        Root<Employee> from = criteria.from(Employee.class);
        criteria.select(from);

        Predicate and = getBuilder().and(getBuilder().equal(from.get("email"), email), getBuilder().equal(from.get("password"), password));
        criteria.where(and);

        return entityManager.createQuery(criteria).getSingleResult();
    }

    public Employee findById(int id) throws NoResultException {
        CriteriaQuery<Employee> criteria = this.getQuery();

        Root<Employee> from = criteria.from(Employee.class);
        criteria.select(from);
        criteria.where(getBuilder().equal(from.get("id"), id));

        return entityManager.createQuery(criteria).getSingleResult();
    }

    public List<Employee> fetchByRoleId(int roleId) throws NoResultException {
        CriteriaQuery<Employee> criteria = this.getQuery();

        Root<Employee> from = criteria.from(Employee.class);
        Join<Employee, Role> role = from.join("role");
        criteria.select(from);
        criteria.where(getBuilder().equal(role.get("id"), roleId));

        return entityManager.createQuery(criteria).getResultList();
    }

    public List<Employee> fetchBySuperiorId(int superiorId) throws NoResultException {
        CriteriaQuery<Employee> criteria = this.getQuery();

        Root<Employee> from = criteria.from(Employee.class);
        Join<Employee, Employee> superior = from.join("superior");
        criteria.select(from);
        criteria.where(getBuilder().equal(superior.get("id"), superiorId));

        return entityManager.createQuery(criteria).getResultList();
    }

    public Employee findByIdAndRole(int id, int roleId) throws NoResultException {
        CriteriaQuery<Employee> criteria = this.getQuery();

        Root<Employee> from = criteria.from(Employee.class);
        Join<Employee, Role> role = from.join("role");
        criteria.select(from);
        Predicate and = getBuilder().and(getBuilder().equal(from.get("id"), id), getBuilder().equal(role.get("id"), roleId));
        criteria.where(and);

        return entityManager.createQuery(criteria).getSingleResult();
    }

    public List<Employee> fetchAll() throws NoResultException {
        CriteriaQuery<Employee> criteria = this.getQuery();

        Root<Employee> from = criteria.from(Employee.class);
        criteria.select(from);

        return entityManager.createQuery(criteria).getResultList();
    }

    public List<Employee> find(String term, int idRole) throws NoResultException {
        CriteriaQuery<Employee> criteria = this.getQuery();

        Root<Employee> from = criteria.from(Employee.class);
        Join<Employee, Role> role = from.join("role");
        criteria.select(from);

        Predicate like = getBuilder().like(from.get("name"), "%"+term+"%");
        Predicate roleEqual = getBuilder().equal(role.get("id"), idRole);

        criteria.where(getBuilder().and(like, roleEqual));

        return entityManager.createQuery(criteria).getResultList();
    }

    public List<Employee> find(String term) throws NoResultException {
        CriteriaQuery<Employee> criteria = this.getQuery();

        Root<Employee> from = criteria.from(Employee.class);
        criteria.select(from);

        Predicate like = getBuilder().like(from.get("name"), "%"+term+"%");
        criteria.where(like);

        return entityManager.createQuery(criteria).getResultList();
    }

    public void update(Employee employee) {
        this.entityManager.merge(employee);
        this.entityManager.flush();
    }

}
