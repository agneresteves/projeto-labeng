package controller;

import form.LoginForm;
import model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import service.EmployeeService;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;

@Controller
public class LoginController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/login")
    public ModelAndView login(LoginForm loginForm) {
        HashMap<String, Object> params = new HashMap<String, Object>();

        params.put("correctLogin", true);

        return new ModelAndView("login", params);
    }

    @PostMapping("/login")
    public ModelAndView login(@Valid LoginForm loginForm, BindingResult bindingResult, HttpSession session, HttpServletResponse response) {
        HashMap<String, Object> params = new HashMap<String, Object>();

        System.out.println("Has errors: " + bindingResult.hasErrors() + " " + loginForm.getEmail() + " " + loginForm.getPassword());

        if (bindingResult.hasErrors()) {
            return new ModelAndView("login");
        }

        Employee employeeLogin = employeeService.login(loginForm.getEmail(), loginForm.getPassword());

        if (employeeLogin != null) {
            session.setAttribute("employee", employeeLogin);
            return new ModelAndView("redirect:/painel");
        }

        params.put("correctLogin", false);

        return new ModelAndView("login", params);
    }

}
