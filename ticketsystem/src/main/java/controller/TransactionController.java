package controller;

import form.AddToolsForm;
import model.Employee;
import model.Transaction;
import model.TransactionItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import service.EmployeeService;
import service.ItemService;
import service.TransactionItemService;
import service.TransactionService;

import javax.servlet.http.HttpSession;
import java.util.HashMap;

@Controller
@RequestMapping("/inventario")
public class TransactionController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private ItemService itemService;

    @Autowired
    private TransactionItemService transactionItemService;

    @Autowired
    private TransactionService transactionService;

    public HashMap<String, Object> getParamForm(String error, String success) {
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("error", error);
        params.put("success", success);
        params.put("employees", employeeService.fetchByRoleId(EmployeeService.TECHNICAL));
        params.put("items", itemService.fetchAllNotDeleted());
        return params;
    }

    @GetMapping("/liberar")
    public ModelAndView getAdd() {
        HashMap<String, Object> params = this.getParamForm("", "");;

        return new ModelAndView("transaction/add", params);
    }

    @PostMapping("/liberar")
    public ModelAndView postAdd(@RequestParam(value="id", defaultValue = "0") int id,
                                AddToolsForm addToolsForm, HttpSession httpSession) {
        HashMap<String, Object> params = this.getParamForm("", "");

        params.put("idEmployee", id);

        Transaction transaction = addToolsForm.getTransaction();
        transaction.setOperation(TransactionService.ADD);
        int idTransaction = transactionService.add(transaction);

        Employee employeeLogged = (Employee) httpSession.getAttribute("employee");
        transaction.setDoEmployee(employeeLogged);

        transaction.setId(idTransaction);
        transaction.setWarehouse(true);

        for (TransactionItem transactionItem : addToolsForm.getTransactionItems()) {
            if (transactionItem.getQuantity() > 0) {
                transactionItem.setTransaction(transaction);
                transactionItemService.add(transactionItem);
            }
        }

        params.put("complete", true);

        params.put("success", "Ferramentas liberadas com sucesso.");

        return new ModelAndView("transaction/add", params);
    }

    @GetMapping("/baixa")
    public ModelAndView getBaixa() {
        HashMap<String, Object> params = this.getParamForm("", "");;
        return new ModelAndView("transaction/baixa", params);
    }

    @PostMapping("/baixa")
    public ModelAndView postBaixa(AddToolsForm addToolsForm, HttpSession httpSession) {
        HashMap<String, Object> params = this.getParamForm("", "");

        Transaction transaction = addToolsForm.getTransaction();
        transaction.setOperation(TransactionService.REMOVE);
        int id = transactionService.add(transaction);

        Employee employeeLogged = (Employee) httpSession.getAttribute("employee");
        transaction.setDoEmployee(employeeLogged);

        transaction.setId(id);
        transaction.setWarehouse(true);

        for (TransactionItem transactionItem : addToolsForm.getTransactionItems()) {
            if (transactionItem.getQuantity() > 0) {
                transactionItem.setTransaction(transaction);
                transactionItemService.add(transactionItem);
            }
        }

        params.put("success", "Ferramentas liberadas com sucesso.");
        params.put("complete", true);

        return new ModelAndView("transaction/baixa", params);
    }

}
