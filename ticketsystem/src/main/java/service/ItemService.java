package service;

import dao.ItemDAO;
import model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ItemService {

    @Autowired
    private ItemDAO itemDAO;

    public List<Item> fetchAll() {
        try {
            return itemDAO.fetchAll(true);
        } catch(NoResultException e) {
            return new ArrayList<>();
        }
    }

    public Item fetchById(int id) {
        try {
            return itemDAO.fetchById(id);
        } catch(NoResultException e) {
            return null;
        }
    }

    public List<Item> fetchAllNotDeleted() {
        try {
            return itemDAO.fetchAll(false);
        } catch(NoResultException e) {
            return new ArrayList<>();
        }
    }

    public void update(Item item) {
        this.itemDAO.update(item);
    }

    public void add(Item item) {
        this.itemDAO.add(item);
    }

    public void softDelete(Item item) {
        item.setDeletedAt(new Date());
        this.itemDAO.update(item);
    }

    public void restore(Item item) {
        item.setDeletedAt(null);
        this.itemDAO.update(item);
    }

}
