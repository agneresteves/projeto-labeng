package service;

import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;

@Service
public class CookieService {

    public Cookie createCookieLogin(String email) {
        Cookie cookie = new Cookie("email", email);
        cookie.setMaxAge(86400000);
        return cookie;
    }

}
