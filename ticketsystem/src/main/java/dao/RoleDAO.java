package dao;

import model.Role;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Transactional
@Repository
public class RoleDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public void add(Role role) {
        this.entityManager.persist(role);
    }

    public List<Role> fetchAll() throws NoResultException {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Role> criteria = builder.createQuery(Role.class);
        Root<Role> from = criteria.from(Role.class);
        criteria.select(from);
        return entityManager.createQuery(criteria).getResultList();
    }

    public Role fetchById(int id) throws NoResultException {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Role> criteria = builder.createQuery(Role.class);
        Root<Role> from = criteria.from(Role.class);
        criteria.select(from);
        criteria.where(builder.equal(from.get("id"), id));
        return entityManager.createQuery(criteria).getSingleResult();
    }

}
