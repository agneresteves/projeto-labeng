package controller;

import model.Employee;
import model.Item;
import model.Transaction;
import model.TransactionItem;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import service.EmployeeService;
import service.RoleService;
import service.TransactionItemService;
import service.TransactionService;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
@RequestMapping("/funcionarios")
public class EmployeeController {

    @Autowired
    private RoleService roleService;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private TransactionItemService transactionItemService;

    @GetMapping("")
    public ModelAndView getListAll() {
        HashMap<String, Object> params = new HashMap<String, Object>();

        params.put("employees", this.employeeService.fetchAll());

        return new ModelAndView("employee/list", params);
    }

    @GetMapping("/{id}")
    public ModelAndView getListByRole(@PathVariable(value="id") int id) {
        HashMap<String, Object> params = new HashMap<String, Object>();

        params.put("employees", this.employeeService.fetchByRoleId(id));
        params.put("role", roleService.fetchById(id));

        return new ModelAndView("employee/list", params);
    }

    @GetMapping("/tecnico/{id}")
    public ModelAndView getSeeTechnical(@PathVariable(value="id") int id) {
        HashMap<String, Object> params = new HashMap<String, Object>();

        List<Transaction> transactionList = transactionService.fetchByToEmployee(id);

        List<TransactionItem> transactionItems = transactionItemService.sumItems(transactionList);

        Employee employee = this.employeeService.findByIdAndRole(id, EmployeeService.TECHNICAL);
        params.put("employee", employee);
        params.put("transactions", transactionList);
        params.put("transactionsItems", transactionItems);

        return new ModelAndView("employee/technical/see", params);
    }

    @GetMapping("/perfil")
    public ModelAndView getProfile(HttpSession httpSession) {
        HashMap<String, Object> params = this.getParamsForRegister("", "");

        Employee employee = (Employee) httpSession.getAttribute("employee");
        params.put("employee", employee);
        params.put("profile", true);
        return new ModelAndView("employee/edit", params);
    }

    @PostMapping("/perfil")
    public ModelAndView postProfile(Employee employee, HttpSession httpSession) {
        Employee employeeLogged = (Employee) httpSession.getAttribute("employee");

        HashMap<String, Object> params = this.executeUpdateEmployee(employeeLogged.getId(), employee, true, httpSession);
        return new ModelAndView("employee/edit", params);
    }

    private HashMap<String, Object> getParamsForRegister(String error, String success) {
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("roles", roleService.fetchAll());
        params.put("supervisors", employeeService.fetchByRoleId(EmployeeService.SUPERVISOR));
        params.put("coordinators", employeeService.fetchByRoleId(EmployeeService.COORDINATOR));
        params.put("error", error);
        params.put("success", success);

        return params;
    }

    @GetMapping("/novo")
    public ModelAndView getRegister() {
        HashMap<String, Object> params = this.getParamsForRegister("", "");
        return new ModelAndView("employee/register", params);
    }

    @PostMapping("/novo")
    public ModelAndView postRegister(Employee employee, HttpSession httpSession) {
        String success = "";
        String error = "";


        if (employee.getConfirmPassword().equals(employee.getPassword())) {
            Employee employeeLogged = (Employee) httpSession.getAttribute("employee");

            if (employeeLogged.getRole().getId() == EmployeeService.SUPERVISOR) {
                employeeLogged.setSuperior(employeeLogged);
            }

            employeeService.add(employee);
            success = "Funcionário cadastrado com sucesso.";
        } else {
            error = "As senhas não se coincidem.";
        }

        HashMap<String, Object> params = this.getParamsForRegister(error, success);
        return new ModelAndView("employee/register", params);
    }

    @GetMapping("/editar/{id}")
    public ModelAndView getEdit(@PathVariable(value="id") int id) {
        HashMap<String, Object> params = this.getParamsForRegister("", "");

        Employee employee = this.employeeService.findById(id);
        params.put("employee", employee);

        return new ModelAndView("employee/edit", params);
    }

    @PostMapping("/editar/{id}")
    public ModelAndView postEdit(@PathVariable(value="id") int id, Employee employee, HttpSession httpSession) {
        HashMap<String, Object> params = this.executeUpdateEmployee(id, employee, false, httpSession);
        return new ModelAndView("employee/edit", params);
    }

    private HashMap<String, Object> executeUpdateEmployee(int id, Employee employee, boolean profile, HttpSession httpSession) {
        HashMap<String, Object> params = this.getParamsForRegister("", "");

        if (!employee.getPassword().isEmpty()) {

            if (!employee.getPassword().equals(employee.getConfirmPassword())) {
                params.put("error", "As senhas não se coincidem.");
            } else {
                this.employeeService.update(employee, true);
                params.put("success", profile ? "Perfil editado com sucesso." : "Funcionário atualizado com sucesso.");

                if (profile) {
                    httpSession.setAttribute("employee", employee);
                }
            }

        } else {
            this.employeeService.update(employee, false);
            params.put("success", profile ? "Perfil editado com sucesso." : "Funcionário atualizado com sucesso.");

            if (profile) {
                httpSession.setAttribute("employee", employee);
            }
        }

        params.put("profile", profile);
        params.put("employee", employee);
        return params;
    }

    @GetMapping("/desativar/{id}")
    public ModelAndView getDesativar(@PathVariable(value="id") int id) {
        Employee employee = employeeService.findById(id);

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("type", "funcionário");

        if (employee != null) {
            params.put("message", "Você realmente deseja desativar o funcionário \"" + employee.getName() + "\"?");
            params.put("has", employee != null);
            params.put("linkNo", "/funcionarios");
        }

        return new ModelAndView("dash/disable", params);
    }

    @PostMapping("/desativar/{id}")
    public ModelAndView postDesativar(@PathVariable(value="id") int id) {
        Employee employee = employeeService.findById(id);

        if (employee == null) {
            return new ModelAndView("redirect:/funcionarios/desativar");
        } else {
            employeeService.softDelete(employee);
            return new ModelAndView("redirect:/funcionarios?disable=true");
        }
    }

    @GetMapping("/ativar/{id}")
    public ModelAndView getAtivar(@PathVariable(value="id") int id) {
        Employee employee = employeeService.findById(id);

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("type", "funcionário");

        if (employee != null) {
            params.put("message", "Você realmente deseja ativar o funcionário \"" + employee.getName() + "\"?");
            params.put("has", employee != null);
            params.put("linkNo", "/funcionarios");
        }

        return new ModelAndView("dash/enable", params);
    }

    @PostMapping("/ativar/{id}")
    public ModelAndView postAtivar(@PathVariable(value="id") int id) {
        Employee employee = employeeService.findById(id);

        if (employee == null) {
            return new ModelAndView("redirect:/funcionarios/ativar");
        } else {
            employeeService.restore(employee);
            return new ModelAndView("redirect:/funcionarios?enable=true");
        }
    }

}
