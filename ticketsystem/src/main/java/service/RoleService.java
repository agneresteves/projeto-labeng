package service;

import dao.RoleDAO;
import model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@Service
public class RoleService {

    @Autowired
    private RoleDAO roleDAO;

    public List<Role> fetchAll() {
        try {
            return roleDAO.fetchAll();
        } catch(NoResultException e) {
            return new ArrayList<Role>();
        }
    }

    public Role fetchById(int id) {
        try {
            return roleDAO.fetchById(id);
        } catch (NoResultException e) {
            return null;
        }
    }

}
