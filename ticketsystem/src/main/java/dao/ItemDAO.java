package dao;

import model.Item;
import model.Role;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Transactional
@Repository
public class ItemDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public void add(Item item) {
        this.entityManager.persist(item);
    }

    public List<Item> fetchAll(boolean all) throws NoResultException {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Item> criteria = builder.createQuery(Item.class);
        Root<Item> from = criteria.from(Item.class);
        criteria.select(from);

        if (!all) {
            criteria.where(builder.isNull(from.get("deletedAt")));
        }

        return entityManager.createQuery(criteria).getResultList();
    }

    public Item fetchById(int id) throws NoResultException {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Item> criteria = builder.createQuery(Item.class);
        Root<Item> from = criteria.from(Item.class);
        criteria.select(from);
        criteria.where(builder.equal(from.get("id"), id));
        return entityManager.createQuery(criteria).getSingleResult();
    }

    public void update(Item item) {
        this.entityManager.merge(item);
        this.entityManager.flush();
    }

}
