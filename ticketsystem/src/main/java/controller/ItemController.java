package controller;

import form.AddToolsForm;
import model.Item;
import model.Transaction;
import model.TransactionItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import service.*;

import java.util.HashMap;

@Controller
@RequestMapping("/itens")
public class ItemController {

    @Autowired
    private ItemService itemService;

    @Autowired
    private MeasureService measureService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("")
    public ModelAndView getHome(@RequestParam(value="disable", defaultValue = "false") boolean disable,
                                @RequestParam(value="enable", defaultValue = "false") boolean enable) {
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("items", itemService.fetchAll());
        params.put("disable", disable);
        params.put("enable", enable);

        return new ModelAndView("items/home", params);
    }

    private HashMap<String, Object> getParamsForm(String error, String success, int id) {
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("measures", measureService.fetchAll());
        params.put("categories", categoryService.fetchAll());
        params.put("error", error);
        params.put("success", success);
        if (id != 0){
            params.put("item", this.itemService.fetchById(id));
        }
        return params;
    }

    private HashMap<String, Object> getParamsForm(String error, String success) {
        return this.getParamsForm(error, success, 0);
    }

    @GetMapping("/novo")
    public ModelAndView getNew() {
        HashMap<String, Object> params = this.getParamsForm("", "");
        return new ModelAndView("items/new", params);
    }

    @PostMapping("/novo")
    public ModelAndView postNew(Item item) {
        itemService.add(item);
        HashMap<String, Object> params = this.getParamsForm("", "Item cadastrado com sucesso.");

        return new ModelAndView("items/new", params);
    }

    @GetMapping("/editar/{id}")
    public ModelAndView getEdit(@PathVariable(value="id") int id) {
        HashMap<String, Object> params = this.getParamsForm("", "", id);
        return new ModelAndView("items/edit", params);
    }

    @PostMapping("/editar/{id}")
    public ModelAndView postEdit(@PathVariable(value="id") int id, Item item) {
        itemService.update(item);
        HashMap<String, Object> params = this.getParamsForm("", "Item editado com sucesso.");
        params.put("item", item);
        return new ModelAndView("items/edit", params);
    }

    @GetMapping("/desativar/{id}")
    public ModelAndView getDesativar(@PathVariable(value="id") int id) {
        Item item = itemService.fetchById(id);

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("type", "Item");

        if (item != null) {
            params.put("message", "Você realmente deseja desativar o item \"" + item.getName() + "\"?");
            params.put("has", item != null);
            params.put("linkNo", "/itens");
        }

        return new ModelAndView("dash/disable", params);
    }

    @PostMapping("/desativar/{id}")
    public ModelAndView postDesativar(@PathVariable(value="id") int id) {
        Item item = itemService.fetchById(id);

        if (item == null) {
            return new ModelAndView("redirect:/itens/desativar");
        } else {
            itemService.softDelete(item);
            return new ModelAndView("redirect:/itens?disable=true");
        }
    }

    @GetMapping("/ativar/{id}")
    public ModelAndView getAtivar(@PathVariable(value="id") int id) {
        Item item = itemService.fetchById(id);

        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("type", "Item");

        if (item != null) {
            params.put("message", "Você realmente deseja ativar o item \"" + item.getName() + "\"?");
            params.put("has", item != null);
            params.put("linkNo", "/itens");
        }

        return new ModelAndView("dash/enable", params);
    }

    @PostMapping("/ativar/{id}")
    public ModelAndView postAtivar(@PathVariable(value="id") int id) {
        Item item = itemService.fetchById(id);

        if (item == null) {
            return new ModelAndView("redirect:/itens/ativar");
        } else {
            itemService.restore(item);
            return new ModelAndView("redirect:/itens?enable=true");
        }
    }



}
