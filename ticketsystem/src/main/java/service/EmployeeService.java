package service;

import dao.EmployeeDAO;
import model.Employee;
import model.Transaction;
import model.TransactionItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class EmployeeService {

    public static final int TECHNICAL = 1;
    public static final int SUPERVISOR = 2;
    public static final int MAGAZZINO = 3;
    public static final int COORDINATOR = 4;

    @Autowired
    private EmployeeDAO employeeDAO;

    @Autowired
    private TransactionService transactionService;

    @Autowired
    private TransactionItemService transactionItemService;

    public void add(Employee employee) {
        this.employeeDAO.add(employee);
    }

    public Employee login(String email, String password) {
        try {
            return employeeDAO.findByEmailAndPassword(email, password);
        } catch(NoResultException e) {
            return null;
        }
    }

    public Employee findById(int id) {
        try {
            return this.employeeDAO.findById(id);
        } catch(NoResultException e) {
            return null;
        }
    }

    public Employee findByIdAndRole(int id, int roleId) {
        try {
            return this.employeeDAO.findByIdAndRole(id, roleId);
        } catch(NoResultException e) {
            return null;
        }
    }

    public List<Employee> fetchByRoleId(int roleId) {
        try {
            return this.employeeDAO.fetchByRoleId(roleId);
        } catch(NoResultException e) {
            return new ArrayList<Employee>();
        }
    }

    public List<Employee> fetchBySuperiorId(int roleId) {
        try {
            return this.employeeDAO.fetchBySuperiorId(roleId);
        } catch(NoResultException e) {
            return new ArrayList<Employee>();
        }
    }

    public List<Employee> fetchAll() {
        try {
            return this.employeeDAO.fetchAll();
        } catch(NoResultException e) {
            return new ArrayList<Employee>();
        }
    }

    public void softDelete(Employee employee) {
        employee.setDeletedAt(new Date());
        this.employeeDAO.update(employee);
    }

    public void restore(Employee employee) {
        employee.setDeletedAt(null);
        this.employeeDAO.update(employee);
    }

    public void update(Employee employee, boolean updatePassword) {
        Employee employeeGet = this.findById(employee.getId());

        employeeGet.setName(employee.getName());
        employeeGet.setEmail(employee.getEmail());
        employeeGet.setRole(employee.getRole());
        employeeGet.setSuperior(employee.getSuperior());

        if (updatePassword) {
            employeeGet.setPassword(employee.getPassword());
        }

        this.employeeDAO.update(employeeGet);
    }

    public HashMap<String, Object> getDashParamsByRole(Employee employee) {
        switch(employee.getRole().getId()) {
            case TECHNICAL:
                return this.getDashParamsForTechnical(employee);
            case SUPERVISOR:
                return this.getDashParamsForSupervisor(employee);
            case MAGAZZINO:
                return this.getDashParamsForMagazzino(employee);
            case COORDINATOR:
                return this.getDashParamsForCoordinator(employee);
        }

        return null;
    }

    public HashMap<String, Object> getDashParamsForMagazzino(Employee employee) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("employees", this.fetchByRoleId(TECHNICAL));
        return params;
    }

    public HashMap<String, Object> getDashParamsForCoordinator(Employee employee) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("employees", this.fetchAll());
        return params;
    }

    public HashMap<String, Object> getDashParamsForSupervisor(Employee employee) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("employees", this.fetchBySuperiorId(employee.getId()));
        return params;
    }

    public HashMap<String, Object> getDashParamsForTechnical(Employee employee) {
        HashMap<String, Object> params = new HashMap<>();

        List<Transaction> transactionList = transactionService.fetchByToEmployee(employee.getId());
        List<TransactionItem> transactionItems = transactionItemService.sumItems(transactionList);

        params.put("employee", employee);
        params.put("transactions", transactionList);
        params.put("transactionsItems", transactionItems);

        return params;
    }

    public String getDashViewByRole(int id) {
        switch(id) {
            case TECHNICAL:
                return "employee/dash/technical";
            case SUPERVISOR:
                return "employee/list";
            case MAGAZZINO:
                return "employee/list";
            case COORDINATOR:
                return "employee/list";
        }

        return "";
    }

    public List<Employee> find(String term, Employee employeeLogged) {
        try {
            if (employeeLogged.getRole().getId() == COORDINATOR) {
                return this.employeeDAO.find(term);
            }
            return this.employeeDAO.find(term, this.getRoleIdByEmployee(employeeLogged));
        } catch(NoResultException e) {
            return new ArrayList<>();
        }
    }

    private int getRoleIdByEmployee(Employee employee) {
        switch(employee.getRole().getId()) {
            case SUPERVISOR:
                return TECHNICAL;
            case MAGAZZINO:
                return TECHNICAL;
        }

        return 0;
    }

}
