package service;

import dao.TransactionItemDAO;
import model.Transaction;
import model.TransactionItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionItemService {

    @Autowired
    private TransactionItemDAO transactionItemDAO;

    public void add(TransactionItem transactionItem) {
        this.transactionItemDAO.add(transactionItem);
    }

    public List<TransactionItem> sumItems(List<Transaction> transactionList) {
        List<TransactionItem> ret = new ArrayList<TransactionItem>();

        for (Transaction tr: transactionList) {
            for (TransactionItem transactionItem: tr.getTransactionItemList()) {

                TransactionItem tiList = this.getItemOnList(transactionItem.getItem().getId(), ret);

                if (tiList == null) {
                    tiList = transactionItem;
                    ret.add(tiList);
                }

                if (tr.getOperation().equals(TransactionService.ADD)) {
                    tiList.setQuantityReleased(tiList.getQuantityReleased() + transactionItem.getQuantity());
                } else if (tr.getOperation().equals(TransactionService.REMOVE)) {
                    tiList.setQuantityUsed(tiList.getQuantityUsed() + transactionItem.getQuantity());
                }

            }
        }

        return ret;
    }

    private TransactionItem getItemOnList(int id, List<TransactionItem> ls) {
        for (TransactionItem ti: ls) {
            if (ti.getItem().getId() == id) {
                return ti;
            }
        }
        return null;
    }

}
