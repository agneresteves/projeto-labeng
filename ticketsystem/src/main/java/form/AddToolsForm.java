package form;

import model.Employee;
import model.Transaction;
import model.TransactionItem;

import java.util.List;

public class AddToolsForm {

    private List<TransactionItem> transactionItems;
    private Transaction transaction;

    public List<TransactionItem> getTransactionItems() {
        return transactionItems;
    }

    public void setTransactionItems(List<TransactionItem> transactionItems) {
        this.transactionItems = transactionItems;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }
}
