package dao;

import model.Employee;
import model.Item;
import model.Transaction;
import model.TransactionItem;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class TransactionItemDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public void add(TransactionItem transactionItem) {
        this.entityManager.persist(transactionItem);
    }

}
