package interceptor;

import com.sun.org.apache.xpath.internal.operations.Bool;
import model.Employee;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

public class LoginInterceptor implements HandlerInterceptor {

    private HashMap<String, Boolean> areas;

    public LoginInterceptor() {
        this.areas = new HashMap<String, Boolean>();
        this.areas.put("painel", false);
        this.areas.put("funcionarios", false);
        this.areas.put("login", true);
        this.areas.put("transacao", false);
        this.areas.put("itens", false);
        this.areas.put("error", true);
        this.areas.put("inventario", false);
        this.areas.put("", true);
    }

    @Override
    public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object controller) throws Exception {
        String uri = req.getRequestURI();

        if (req.getSession().getAttribute("employee") == null) {

            String[] partsUrl = uri.replaceFirst("/", "").split("/");
            String start = partsUrl.length == 0 ? "" : partsUrl[0];

            if (!this.areas.get(start)) {
                res.sendRedirect("/login");
                return false;
            }

        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest req, HttpServletResponse res, Object o, ModelAndView modelAndView) throws Exception {
        if (req.getSession().getAttribute("employee") != null) {
            Employee employee = (Employee) req.getSession().getAttribute("employee");
            modelAndView.addObject("employeeLogged", employee);
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }

}
