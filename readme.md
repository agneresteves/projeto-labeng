# Ferramenta para almoxarifado

* Coordenadores podem cadastrar diversos Supervisores e atendentes do almoxarifado
* Supervisores podem cadastrar diversos Técnicos
* Atendentes de almoxarifado podem liberar itens para os ténicos trabalharem
* Os ténicos podem dar baixa em um item dizendo assim que o item foi utilizado

## Pré-requisitos
* Java
* Tomcat
* MySQL
* Uma IDE (Eclipse, IntelliJ IDEA, etc)

## Instalação

1. Baixe o projeto ou faça um clone com o GIT
2. Importe o projeto em sua IDE de preferência (Eclipse, IntelliJ IDEA, etc)
3. Execute o script "database.sql" na raiz do projeto em seu banco de dados MySQL
4. Em sua IDE abre o arquivo "\ticketsystem\src\main\resources\application.properties" e edita as seguintes opções:
	1. spring.datasource.url = Url (padrão 'localhost' com o nome do banco, padrão 'system_tools')
	2. spring.datasource.username = Usuário para conectar ao banco
	3. spring.datasource.password = Senha para conectar ao banco
5. Atualize as dependências com o Maven
6. Execute o método main do projeto que está na classe "controller/Application.java"
7. Acesse "http://localhost:8080" (8080 é a sua porta padrão do tomcat)
8. Acesse o sistema com o usuário:
    * E-mail = coordenador@email.com
    * Senha  = 12345