package service;

import dao.ItemDAO;
import dao.MeasureDAO;
import model.Item;
import model.Measure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@Service
public class MeasureService {

    @Autowired
    private MeasureDAO measureDAO;

    public List<Measure> fetchAll() {
        try {
            return measureDAO.fetchAll();
        } catch(NoResultException e) {
            return new ArrayList<>();
        }
    }

}
