package service;

import dao.CategoryDAO;
import model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryService {

    @Autowired
    private CategoryDAO categoryDAO;

    public List<Category> fetchAll() {
        try {
            return categoryDAO.fetchAll();
        } catch(NoResultException e) {
            return new ArrayList<>();
        }
    }

}
