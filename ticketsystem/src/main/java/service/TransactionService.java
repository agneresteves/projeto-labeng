package service;

import dao.TransactionDAO;
import model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionService {

    public static final String ADD = "+";
    public static final String REMOVE = "-";

    @Autowired
    private TransactionDAO transactionDAO;

    public int add(Transaction t) {
        return transactionDAO.add(t);
    }

    public List<Transaction> fetchAll() {
        try {
            return transactionDAO.fetchAll();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }

    public List<Transaction> fetchByToEmployee(int id) {
        try {
            return transactionDAO.fetchByToEmployee(id);
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }

}
